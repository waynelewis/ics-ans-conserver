ics-ans-conserver
=================

Ansible playbook to install conserver server and client

Variables
---------

Usage
-----

The playbook can be run locally or from an Ansible server.

To run from an Ansible server
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

You must have ansible >= 2.4.0.0 already installed.
You should be able to ssh to the server to install and have sudo rights.

::

    $ git clone https://gitlab.esss.lu.se/waynelewis/ics-ans-conserver.git
    $ cd ics-ans-conserver
    # Edit the "hosts" file
    $ make roles
    $ make playbook


Testing
-------

No testing configured at the moment.

License
-------

BSD 2-clause
